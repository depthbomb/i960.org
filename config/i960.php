<?php

return [


	/**
	 * When to start the "views since ..." value. Currently set to a value
	 * corresponding to the number of views the old site had before this revamp.
	 * You can manually set views by changing this number and then deleting
	 * all files in /storage/framework/cache/data
	 */
	'views_start' => 10240,


	/**
	 * Contextual color classes chosen at random for each request to style
	 * various elements.
	 * 
	 * See https://bulma.io/documentation/overview/start/ for more info.
	 */
	'colors' => [
		'is-info', 'is-primary', 'is-danger', 'is-link', 'is-dark', 'is-success'
	],

	
	/**
	* This variable controls the donor command list on the site.
	* Description can contain HTML code, but be careful not to break things.
	* Also make sure to escape ' characters if you use them:  you're   ->   you\'re
	*
	* Set 'usage' to null to indicate that the perk is activated automatically.
	* Otherwise, you can supply the command or custom text.
	*/
	'perks' => [
		[
			'name' => 'AFK Immunity',
			'description' => 'Donators are immune from the AFK check meaning donators will no longer be moved to spectator or kicked from the server for being AFK.',
			'usage' => null,
		],
		[
			'name' => 'Autobalance Immunity',
			'description' => 'Donators are immune to the autobalance check meaning donators will no longer be moved to another team when teams are unbalanced.',
			'usage' => null,
		],
		[
			'name' => 'Be the Robot',
			'description' => 'Turn yourself into the MvM robotic version of your class',
			'usage' => 'Accessible through the !donor menu',
		],
		[
			'name' => 'Better Chance at a Good Roll',
			'description' => 'Donators gain an extra 25% chance of rolling a "good" roll.',
			'usage' => null,
		],
		[
			'name' => 'Bumper Car',
			'description' => 'Ride around in a bumper car',
			'usage' => 'Accessible through the !donor menu',
		],
			[
			'name' => 'Changeable Colored Name',
			'description' => 'Donators will receive a colored name for everyone in the server to see that you\'re a donator.<br>This colored name can be changed at any time by the donator by visiting <a href="http://colors.i960.org">http://colors.i960.org</a>.',
			'usage' => null,
		],
		[
			'name' => 'Donator Tag',
			'description' => 'Donators will be given a "[D]" tag for everyone in the server to see that you\'re a donator.',
			'usage' => null,
		],
		[
			'name' => 'Fake Notices',
			'description' => 'Donators will be able to send fake notices to the other players in the server. The commands are as follows:<br><b>/fakebuy [item]</b> - Donator has purchased: [item]<br><b>/fakecraft [item]</b> - Donator has crafted: [item]<br><b>/fakeearn [item]</b> - Donator has earned: [item]<br><b>/fakefind [item]</b> - Donator has found: [item]<br><b>/fakereceive [item]</b> - Donator has received a gift: [item]<br><b>/faketrade [item]</b> - Donator has traded for: [item]<br><b>/fakeunbox [item]</b> - Donator has unboxed: [item]<br><u>Each donator is allowed to use each command once every 24 hours.</u>',
			'usage' => 'See description',
		],
		[
			'name' => 'Halloween Footsteps',
			'description' => 'Use any Halloween footprint effect.',
			'usage' => 'Accessible through the !donor menu',
		],
		[
			'name' => 'Hats on Buildings',
			'description' => 'Donators are given the privilege of hats being spawned on their buildings as an Engineer.',
			'usage' => null,
		],
		[
			'name' => 'Instant, Instant Respawn',
			'description' => 'Donators will have a 50% quicker respawn time than all other players.',
			'usage' => null,
		],
		[
			'name' => 'Reserved Slot',
			'description' => 'Donators are given a reserved slot meaning you can connect when the server is full. To use, simply type connect <code>tf2.i960.org</code> in the console.',
			'usage' => null,
		],
		[
			'name' => 'Taunts',
			'description' => 'Use any taunt for your class even if you do not own it',
			'usage' => 'Accessible through the !donor menu',
		],
		[
			'name' => 'Unlimited Voice Commands',
			'description' => 'Donators are immune from the voice command limiter meaning you can spam voice commands.',
			'usage' => null,
		],
		[
			'name' => 'Voice Effects',
			'description' => 'Apply effects to your class\'s voice commands.',
			'usage' => 'Accessible through the !donor menu',
		],
	],

	/**
	 * Don't touch
	 */
	'commit_endpoint' => 'https://api.bitbucket.org/2.0/repositories/depthbomb/i960.org/commits'
];
