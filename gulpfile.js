const fs = require('fs-extra');
const path = require('path');
const util = require('gulp-util');
const gulp = require('gulp');
const merge = require('merge-stream');
const csso = require('gulp-csso');
const uglify = require('gulp-uglify');
const jsonmin = require('gulp-jsonminify');
const base64 = require('gulp-css-base64');
const imagemin = require('gulp-imagemin');
const header = require('gulp-header');
const sass = require('gulp-sass');
const autoprefix = require('gulp-autoprefixer');
const replace = require('gulp-replace');
const rename = require('gulp-rename');
const rev = require('gulp-rev');
const revFormat = require('gulp-rev-format');
const revReplace = require('gulp-rev-replace');
const md5 = require('md5');
const uuidv4 = require('uuid/v4');
const glob = require('glob');

const PRODUCTION = util.env.env === 'prod' ? true : false;

const settings = {
	imageQuality: PRODUCTION ? 3 : 0
};

const resourcesDir = path.join(__dirname, 'resources', 'assets');
const buildDir = path.join(__dirname, 'public');
const build_hash = md5(Math.random());
const build_hash_short = build_hash.substring(0, 8);
const build_date = Math.round(new Date().getTime() / 1000);
const pkg = require('./package.json');

const license = [
	"/*",
	"* This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. ",
	"* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.",
	`* Made for i960.org by Caprine.net (Copyright (c) 2015 - ${(new Date()).getFullYear()} Caprine Softworks)`,
	"*/",
	""
].join("\n");

const buildConfig = {
	baseUrl: path.join(buildDir, '/')
};


const reverse = (string) => {
	// Step 1. Create an empty string that will host the new created string
	var newString = "";

	// Step 2. Create the FOR loop
	 /* The starting point of the loop will be (str.length - 1) which corresponds to the 
		 last character of the string, "o"
		 As long as i is greater than or equals 0, the loop will go on
		 We decrement i after each iteration */
	for (var i = string.length - 1; i >= 0; i--) {
		newString += string[i]; // or newString = newString + str[i];
	}
	 /* Here hello's length equals 5
		  For each iteration: i = str.length - 1 and newString = newString + str[i]
		  First iteration:    i = 5 - 1 = 4,         newString = "" + "o" = "o"
		  Second iteration:   i = 4 - 1 = 3,         newString = "o" + "l" = "ol"
		  Third iteration:    i = 3 - 1 = 2,         newString = "ol" + "l" = "oll"
		  Fourth iteration:   i = 2 - 1 = 1,         newString = "oll" + "e" = "olle"
		  Fifth iteration:    i = 1 - 1 = 0,         newString = "olle" + "h" = "olleh"
	 End of the FOR Loop*/

	// Step 3. Return the reversed string
	return newString; // "olleh"
};


const uid = (length) => {
	const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
	let text = "";
	for (let i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
};


gulp.task('license', () => {
	return gulp.src([
		buildDir + '/assets/js/*.js',
		buildDir + '/assets/css/*.css',
	], { base: buildDir + '/assets' })
	.pipe(header(license))
	.pipe(gulp.dest(buildDir + '/assets'));
});


gulp.task('sass', function() {
	gulp.src([
		resourcesDir + '/sass/app.scss',
	])
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefix({
		browsers: 'last 50 versions, last 30 iOS versions, last 30 Android versions, last 20 bb versions, last 20 and_chr versions, ie 6-9, > 1%'
	}))
	.pipe(csso())
	.pipe(base64({
		baseDir: './public',
		extensionsAllowed: ['.png', '.jpg', 'jpeg', '.bmp', '.svg', '.gif']
	}))
	.pipe(rev())
	.pipe(rename(p => {
		p.basename = `${uuidv4()}`;
	}))
	.pipe(gulp.dest(buildDir + '/assets/css'))
	.pipe(rev.manifest('manifest.json'))
	.pipe(gulp.dest(buildDir + '/assets/css'));
});

gulp.task('images', () => {
	const app = gulp.src([
		resourcesDir + '/img/app/*.{jpeg,jpg,gif,png,bmp,svg}'
	])
	.pipe(imagemin([
		imagemin.gifsicle({ interlaced: true }),
		imagemin.jpegtran({ progressive: true }),
		imagemin.optipng({ optimizationLevel: settings.imageQuality }),
		imagemin.svgo({
			plugins: [
				{ removeViewBox: true },
				{ cleanupIDs: false }
			]
		})
	], { verbose: true }))
	.pipe(rev())
	.pipe(rename(p => {
		p.basename = `${uuidv4()}`;
	}))
	.pipe(gulp.dest(buildDir + '/assets/img/app'))
	.pipe(rev.manifest('manifest.json'))
	.pipe(gulp.dest(buildDir + '/assets/img/app'));

	const ui = gulp.src([
		resourcesDir + '/img/ui/*.{jpeg,jpg,gif,png,bmp,svg}'
	])
	.pipe(imagemin([
		imagemin.gifsicle({ interlaced: true }),
		imagemin.jpegtran({ progressive: true }),
		imagemin.optipng({ optimizationLevel: settings.imageQuality }),
		imagemin.svgo({
			plugins: [
				{ removeViewBox: true },
				{ cleanupIDs: false }
			]
		})
	], { verbose: true }))
	.pipe(rev())
	.pipe(rename(p => {
		p.basename = `${uuidv4()}`;
	}))
	.pipe(gulp.dest(buildDir + '/assets/img/ui'))
	.pipe(rev.manifest('manifest.json'))
	.pipe(gulp.dest(buildDir + '/assets/img/ui'));
	
	const vendor = gulp.src([
		resourcesDir + '/img/vendor/*.{jpeg,jpg,gif,png,bmp,svg}',
		resourcesDir + '/img/vendor/lightbox/*.{jpeg,jpg,gif,png,bmp,svg}'
	])
	.pipe(imagemin([
		imagemin.gifsicle({ interlaced: true }),
		imagemin.jpegtran({ progressive: true }),
		imagemin.optipng({ optimizationLevel: settings.imageQuality }),
		imagemin.svgo({
			plugins: [
				{ removeViewBox: true },
				{ cleanupIDs: false }
			]
		})
	], { verbose: true }))
	.pipe(rev())
	.pipe(rename(p => {
		p.basename = `${uuidv4()}`;
	}))
	.pipe(gulp.dest(buildDir + '/assets/img/vendor'))
	.pipe(rev.manifest('manifest.json'))
	.pipe(gulp.dest(buildDir + '/assets/img/vendor'));

	return merge(ui, app, vendor);
});


gulp.task('font', () => {
	const core = gulp.src([
		resourcesDir + '/font/roboto/*.{ttf,woff,woff2,eot,svg,otf}'
	])
	.pipe(rev())
	.pipe(rename(p => {
		p.basename = `${uuidv4()}`;
	}))
	.pipe(gulp.dest(buildDir + '/assets/font/roboto'))
	.pipe(rev.manifest('manifest.json'))
	.pipe(gulp.dest(buildDir + '/assets/font/roboto'));

	const mdi = gulp.src([
		resourcesDir + '/font/materialdesignicons/*.{ttf,woff,woff2,eot,svg,otf}'
	])
	.pipe(rev())
	.pipe(rename(p => {
		p.basename = `${uuidv4()}`;
	}))
	.pipe(gulp.dest(buildDir + '/assets/font/mdi'))
	.pipe(rev.manifest('manifest.json'))
	.pipe(gulp.dest(buildDir + '/assets/font/mdi'));

	return merge(core, mdi);
});


gulp.task('cleanup', ['cleanupThumbsDB'], () => {
	glob(`${buildDir}/assets/**/*.{jpg,gif,png,svg,mp3,db,css,js,mp4,webm,wav,otf,ttf,eot,woff,woff2}!(assets.json)`, (e, f) => {
		f.forEach(file => {
			fs.unlink(path.resolve('./', file), (err) => {
				if (err && err.code != 'ENOENT') throw new Error(err);
				return util.log(`Cleared built file:`, file);
			});
		});
	});
});


gulp.task('cleanupThumbsDB', () => {
	glob(`${resourcesDir}/**/*.db`, (e, f) => {
		f.forEach(file => {
			fs.unlink(path.join('./', file), (err) => {
				if (err && err.code != 'ENOENT') throw new Error(err);
				return util.log(`Deleted DB file:`, file);
			});
		});
	});

	glob(`./public/**/*.db`, (e, f) => {
		f.forEach(file => {
			fs.unlink(path.join('./', file), (err) => {
				if (err && err.code != 'ENOENT') throw new Error(err);
				return util.log(`Deleted DB file:`, file);
			});
		});
	});
});


gulp.task('rev', () => {
	const manifest = gulp.src([
		buildDir + '/assets/**/**/manifest.json'
	]);

	return gulp.src([
		`${buildDir}/**/*.{css,js}`
	])
	.pipe(revReplace({ manifest }))
	.pipe(gulp.dest(buildDir));
});


gulp.task('rev_js', () => {
	return gulp.src([
		buildDir + '/assets/js/app.js'
	])
	.pipe(rev())
	.pipe(rename(p => {
		p.basename = `${uuidv4()}`;
	}))
	.pipe(gulp.dest(buildDir + '/assets/js'))
	.pipe(rev.manifest('manifest.json'))
	.pipe(gulp.dest(buildDir + '/assets/js'));
});


gulp.task('default', ['sass', 'images', 'font']);