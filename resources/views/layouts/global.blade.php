<!DOCTYPE html>
<html lang="en">
	@include('components/credit')
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>i960.org</title>
		@include('components/head')
	</head>
	<body>
		@include('components/navbar')
		@include('components/hero', ['bgMin' => 1, 'bgMax' => 9])

		@include('components/sections/servers')
		@include('components/sections/donate')

		@include('components/footer')
	</body>
</html>