<header class="navbar {{ randomColor(config('i960.colors')) }}">
	<div class="container">
		<div id="navigation" class="navbar-menu">
			<div class="navbar-start">
				<a href="#servers" class="navbar-item">
					<span class="icon icon-server"></span> Servers
				</a>
				<a class="navbar-item" href="http://discord.i960.org">
					<span class="icon icon-discord"></span> Discord
				</a>
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link" href="javascript:;">
						<span class="icon icon-heart"></span> Donor
					</a>
					<div class="navbar-dropdown">
						<a class="navbar-item" href="#donate">
							Donor Perks
						</a>
						<a class="navbar-item" href="#donateNow">
							Donate Now
						</a>
						<a class="navbar-item" href="https://steamcommunity.com/groups/i960/discussions/0/38596748209771138/" target="_blank">
							Free perks
						</a>
					</div>
				</div>
			</div>
			<div class="navbar-end">
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link" href="javascript:;">
						<span class="icon icon-link-variant"></span> Links
					</a>
					<div class="navbar-dropdown">
						<a class="navbar-item" href="https://i960.org/sourcebans">
							SourceBans
						</a>
						<a class="navbar-item" href="http://stats.i960.org/">
							Ranks
						</a>
						<a class="navbar-item" href="https://steamcommunity.com/groups/i960" target="_blank">
							Steam Group
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>