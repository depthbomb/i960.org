<section id="donate" class="section">
	<div class="container">
		<h1 class="title">i960.org offers special perks for those that donate.</h1>

		{{--
			This donor perk table is automatically generated! Avoid directly editing it here. You can edit the perk info in the /config/i960.php file!
		--}}
		<table id="donorPerks" class="table is-bordered is-striped is-hoverable is-fullwidth">
			<thead>
				<tr>
					<th>#</th>
					<th>Perk</th>
					<th>Description</th>
					<th>Usage</th>
				</tr>
			</thead>
			<tbody>
				@foreach(config('i960.perks') as $i => $perk)
				<tr>
					<td>{{ $i+1 }}</td>
					<td>{{ $perk['name'] }}</td>
					<td>{!! $perk['description'] !!}</td>
					<td>{{ $perk['usage'] !== null ? $perk['usage'] : 'Automatic' }}</td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Perk</th>
					<th>Description</th>
					<th>Usage</th>
				</tr>
			</tfoot>
		</table>

		<a id="donateNow" href="https://www.i960.org/donate" class="button is-rounded is-success is-large is-block is-halfwidth is-mcentered my-5">
			<span class="icon icon-currency-usd"></span> Donate now!
		</a>
	</div>
</section>