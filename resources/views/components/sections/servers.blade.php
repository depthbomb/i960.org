<section id="servers" class="section">
	<div class="container-fluid">
		<div class="tile is-ancestor">
			<div class="tile is-vertical is-6">
				<div class="tile">
					<div class="tile is-parent is-vertical">
						<article id="mk2" class="tile is-child notification {{ randomColor(config('i960.colors')) }}">
							<p class="title">Loading&hellip;</p>
							<p class="subtitle">&hellip;</p>
						</article>
					</div>
				</div>
			</div>
			<div class="tile is-vertical is-6">
				<div class="tile">
					<div class="tile is-parent is-vertical">
						<article id="mk4" class="tile is-child notification {{ randomColor(config('i960.colors')) }}">
							<p class="title">Loading&hellip;</p>
							<p class="subtitle">&hellip;</p>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>