<section id="hero" class="hero is-fullheight" style="background-image:url('{{ rev('/assets/img/ui/hero' . rand($bgMin, $bgMax) . '.jpg') }}');">
	<div class="hero-body">
		<div class="container has-text-centered">
			<img src="{{ rev('/assets/img/ui/2018_i960sitelogosvg.svg') }}" alt="i960.org">
		</div>
	</div>
	<div class="hero-overlay"></div>
</section>