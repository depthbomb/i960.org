{{-- EVERYTHING HERE IS INSERTED INTO THE WEBSITE'S <head> --}}
<meta name="description" content="i960.org">
<meta name="theme-color" content="#44f256">
<link rel="manifest" href="{{ route('manifest.json') }}">
<meta name="url" content="{{ route('index') }}">
<meta name="referrer" content="unsafe-url">
<link rel="canonical" content="{{ route('index') }}">

<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="i960.org">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<meta property="og:title" content="i960.org">
<meta property="og:url" content="{{ route('index') }}">
<meta property="og:description" content="i960.org">
<meta property="og:image" content="{{ rev('/assets/img/app/app-144.png') }}">
<meta property="og:image:secure_url" content="{{ rev('/assets/img/app/app-144.png') }}">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="144">
<meta property="og:image:height" content="144">
<meta property="og:type" content="website">

<meta property="og:site_name" content="i960.org">
<meta property="og:locale" content="en_US">

<meta itemprop="name" content="i960.org">
<meta itemprop="url" content="{{ route('index') }}">
<meta itemprop="description" content="i960.org">
<meta itemprop="image" content="{{ rev('/assets/img/app/app-144.png') }}">

<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="i960.org">
<meta name="twitter:url" content="{{ route('index') }}">
<meta name="twitter:description" content="i960.org">
<meta name="twitter:image" content="{{ rev('/assets/img/app/app-144.png') }}">

<meta name="application-name" content="i960.org">
<meta name="msapplication-starturl" content="{{ route('index') }}">
<meta name="msapplication-config" content="{{ route('browserconfig.xml') }}">
<meta name="msapplication-tooltip" content="i960.org">
<meta name="msapplication-TileImage" content="{{ rev('/assets/img/app/app-144.png') }}">
<meta name="msapplication-TileColor" content="#44f256">
<meta name="msapplication-navbutton-color" content="#44f256">

<link rel="apple-touch-icon" sizes="57x57" href="{{ rev('/assets/img/app/app-57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ rev('/assets/img/app/app-60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ rev('/assets/img/app/app-72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ rev('/assets/img/app/app-76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ rev('/assets/img/app/app-114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ rev('/assets/img/app/app-120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ rev('/assets/img/app/app-144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ rev('/assets/img/app/app-152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ rev('/assets/img/app/app-180.png') }}">
<!--[if lt IE 9]><link rel="shortcut icon" type="image/x-icon" href="{{ bust('/favicon.ico') }}">-->
<link rel="icon" type="image/ico" href="{{ bust('/favicon.ico') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ rev('/assets/img/app/app-16.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ rev('/assets/img/app/app-32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ rev('/assets/img/app/app-96.png') }}">
<link rel="icon" type="image/png" sizes="192x192" href="{{ rev('/assets/img/app/app-192.png') }}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="stylesheet" href="{{ rev('/assets/css/app.css') }}" type="text/css">
<script src="{{ rev('/assets/js/app.js') }}"></script>