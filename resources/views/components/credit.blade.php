{{-- This is just a harmless comment put at the very top of all pages. Please leave it in as it is to credit me without being obtrusive!  --}}

<!--

	Built with love by depthbomb 🐐
	http://steamcommunity.com/profiles/76561198026398801

	-------------

	Commit: {{ getCommitHash(config('i960.commit_endpoint')) }}

-->