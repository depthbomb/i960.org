@php

if (cache()->has('views')) {
    cache()->increment('views', 1);
} else {
    cache()->add('views', config('i960.views_start'), now()->addYears(15));
}

@endphp

<footer class="footer">
	<div class="container-fluid">
		<div class="content has-text-centered">
			<p>This page has been viewed <strong>{{ cache()->get('views') }}</strong> times since <strong>July 10<sup>th</sup>, 2014</strong>.</p>
		</div>
	</div>
</footer>