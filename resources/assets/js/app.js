import $ from "jquery";
window.jQuery = $;

window.i960 = window.i960 || {
	Init: () => {

		window.axios = require('axios');
		window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

		let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

		if (token) {
			window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token;
		} else {
			throw new Error('CSRF token not found');
		}

		i960.UI.Init();
	},

	UI: {
		Init: () => {

			i960.UI.InitServerInfo('/api/servers');
		},

		InitServerInfo: (endpoint) => {
			axios.get(endpoint).then(response => {
				const data = response.data;
				const mk2Element = $('#mk2');
				const mk4Element = $('#mk4');

				const elementArray = [
					[mk2Element, 'mk2', data],
					[mk4Element, 'mk4', data]
				];

				function shuffle(array) {
					let currentIndex = array.length, temporaryValue, randomIndex;

					// While there remain elements to shuffle...
					while (0 !== currentIndex) {

						// Pick a remaining element...
						randomIndex = Math.floor(Math.random() * currentIndex);
						currentIndex -= 1;

						// And swap it with the current element.
						temporaryValue = array[currentIndex];
						array[currentIndex] = array[randomIndex];
						array[randomIndex] = temporaryValue;
					}

					return array;
				}

				function fillElement(parent, id, data) {
					const title = parent.find('.title');
					const subtitle = parent.find('.subtitle');
					const serverData = data[id];
					const buttonColors = ['is-info', 'is-primary', 'is-danger', 'is-link', 'is-dark', 'is-success'];
					const subtitleText = `<a href="${serverData.join}" class="button ${shuffle(buttonColors)[0]} is-inverted is-outlined is-small">Connect</a> - Players: ${serverData.players} - Map: ${serverData.map}`;

					title.text(serverData.name);
					subtitle.html(subtitleText);
				}

				elementArray.forEach(s => {
					fillElement(s[0], s[1], s[2]);
				});

			}).catch(error => {
				console.log(error);
			});
		}
	}
}









document.addEventListener("DOMContentLoaded", function() {
	var i960 = window.i960 || {};
	i960.Init();
});