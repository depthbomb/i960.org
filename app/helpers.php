<?php

	if (!function_exists('u')) {
		function u(string $path = '')
		{
			return sprintf(
				'%s://%s%s%s',
				isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
				$_SERVER['SERVER_NAME'],
				isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 8000 ? ":{$_SERVER['SERVER_PORT']}" : '',
				$path
			);
		}
	}

	if (!function_exists('guid')) {
		/**
		 * Generates a GUID
		 *
		 * @param integer $salt				Optional numeric salt
		 * @param boolean $braces			Wrap output in curly braces
		 * @param boolean $uppercase		Returns the string in all uppercase
		 * @param boolean $classFriendly	Makes the GUID usable in HTML classes or IDs
		 */
		function guid(int $salt = null, bool $braces = false, bool $uppercase = false, bool $classFriendly = false)
		{
			srand(is_int($salt) ? $salt : (double) microtime() * 10000 + rand());
			$left_brace = chr(123);
			$right_brace = chr(125);
			$uuid = sprintf(
				'%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

				// 32 bits for "time_low"
				rand(0, 0xffff),
				mt_rand(0, 0xffff),

				// 16 bits for "time_mid"
				rand(0, 0xffff),

				// 16 bits for "time_hi_and_version",
				// four most significant bits holds version number 4
				rand(0, 0x0fff) | 0x4000,

				// 16 bits, 8 bits for "clk_seq_hi_res",
				// 8 bits for "clk_seq_low",
				// two most significant bits holds zero and one for variant DCE1.1
				rand(0, 0x3fff) | 0x8000,

				// 48 bits for "node"
				rand(0, 0xffff),
				rand(0, 0xffff),
				rand(0, 0xffff)
			);

			if ($uppercase) $uuid = strtoupper($uuid);
			if ($braces) $uuid = $left_brace . $uuid . $right_brace;

			return $uuid;
		}
	}

	if (!function_exists('uid')) {
		/**
		 * Generates a "unique ID"
		 *
		 * @param integer Length of the returned ID
		 * @param string Extra characters to include in the generation
		 * @param boolean Remove similar-looking characters
		 */
		function uid(int $length = 8, string $extra = null, bool $readable = false)
		{
			if (null !== $extra) {
				$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_{$extra}";
			} else {
				if ($readable) {
					$characters = 'abcdefghkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789-_';
				} else {
					$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_';
				}
			}
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
	}

	if (!function_exists('px')) {
		function px()
		{
			return 'data:image/gif;base64,R0lGODlhAQABAPAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
		}
	}

	if (!function_exists('timeago')) {
		function timeago(int $timestamp)
		{
			$estimate_time = time() - $timestamp;

			if ($estimate_time < 1) {
				return 'Just Now';
			}

			$condition = [
				12 * 30 * 24 * 60 * 60 => 'year',
				30 * 24 * 60 * 60 => 'month',
				24 * 60 * 60 => 'day',
				60 * 60 => 'hour',
				60 => 'minute',
				1 => 'second',
			];
			foreach ($condition as $secs => $str) {
				$d = $estimate_time / $secs;
				if ($d >= 1) {
					$r = round($d);
					return ' ' . $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
				}
			}
		}
	}

	if (!function_exists('utf8_field')) {
		function utf8_field()
		{
			return new \Illuminate\Support\HtmlString('<input type="hidden" name="utf8" value="&#x2713;">');
		}
	}

	if (!function_exists('bust')) {
		function bust(string $path, bool $fullUrl = true)
		{
			$file = getcwd() . $path;	//	getcwd() should return the path to the public directory

			if (file_exists($file)) {
				$hash = md5(filemtime($file));

				return u("{$path}?{$hash}");
			} else {
				return "!!	ERROR: bust() could not locate file	!!\n";
			}
		}
	}

	if (!function_exists('rev')) {
		/**
		* Asset revision helper
		* @param  string $p			Absolute path to file relative to /public/
		* @param  bool   $pathOnly	Whether to return the versioned asset as just a path or as a full URL
		* @return string
		*/
		function rev($p, $pathOnly = false)
		{
			$path = getcwd() . $p; // public/$p
			$file = basename($path); // returns the file name
			$dir = str_replace($file, '', $path); // gets the directory path to the file
			$public = str_replace($file, '', $p); // returns the public path to the file (from within /public/)
			$rev = str_replace($file, '', $path) . 'manifest.json'; // path to the manifest file that SHOULD reside in the directory of the file being accessed

			if (file_exists($rev)) {
				$manifest = json_decode(file_get_contents($rev), true);
				if (isset($manifest[$file])) {
					$versioned = $manifest[$file];
					$versioned = preg_replace("/\.\//", '/', $versioned);

					if ($pathOnly) {
						return $versioned;
					}

					return u($public . $versioned);
				} else {
					return u($p . '?INX_NOT_FOUND');
				}
			} else {
				return u($p . '?REV_NOT_FOUND');
			}
		}
	}

	function getCommitHash($endpoint) {
		$cache_key = 'commit_hash';

		if (cache()->has($cache_key)) {
			$hash = cache()->get($cache_key);
		} else {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $endpoint);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($ch);
			curl_close($ch);
			
			$data = json_decode($output, true);
			$hash = cache()->remember($cache_key, now()->addDays(1), function() use ($data) {
				return $data['values'][0]['hash'];
			});
		}

		return $hash;
	}

	function randomColor($arr) {
		$random = array_rand($arr);
		return $arr[$random];
	}