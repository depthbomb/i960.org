<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index() {
        return view('deploy/index');
    }






    //  Avoid modifying these two methods
    public function appManifest()
    {
        $manifest = [
            'name' => 'i960.org',
            'short_name' => 'i960',
            'description' => 'i960.org',
            'display' => 'standalone',
            'scope' => '/',
            'start_url' => '/?utm_source=manifest',
            'dir' => 'ltr',
            'lang' => 'en-US',
            'default_locale' => 'en',
            'display' => 'browser',
            'theme_color' => '#44f256',
            'background_color' => '#44f256',
            'icons' => [
                [
                    'src' => rev('/assets/img/app/app-36.png'),
                    'sizes' => '36x36',
                    'type' => 'image/png',
                    'density' => '0.75',
                ],
                [
                    'src' => rev('/assets/img/app/app-48.png'),
                    'sizes' => '48x48',
                    'type' => 'image/png',
                    'density' => '1.0',
                ],
                [
                    'src' => rev('/assets/img/app/app-72.png'),
                    'sizes' => '72x72',
                    'type' => 'image/png',
                    'density' => '1.5',
                ],
                [
                    'src' => rev('/assets/img/app/app-96.png'),
                    'sizes' => '96x96',
                    'type' => 'image/png',
                    'density' => '2.0',
                ],
                [
                    'src' => rev('/assets/img/app/app-144.png'),
                    'sizes' => '144x144',
                    'type' => 'image/png',
                    'density' => '3.0',
                ],
                [
                    'src' => rev('/assets/img/app/app-192.png'),
                    'sizes' => '192x192',
                    'type' => 'image/png',
                    'density' => '4.0',
                ],
            ],
            'related_applications' => [
                [
                    'platform' => 'web',
                    'url' => 'https://i960.org/',
                ],
            ],
        ];

        return $manifest;
    }

    public function browserConfig()
    {
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<browserconfig>
			<msapplication>
				<tile>
					<square70x70logo src="' . rev('/assets/img/app/app-70.png') . '"/>
					<square150x150logo src="' . rev('/assets/img/app/app-150.png') . '"/>
					<wide310x150logo src="' . rev('/assets/img/app/app-310.png') . '"/>
					<square310x310logo src="' . rev('/assets/img/app/app-310.png') . '"/>
					<TileColor>#44f256</TileColor>
				</tile>
			</msapplication>
		</browserconfig>';

        return response($xml, 200)->header('Content-Type', 'text/xml');
    }
}
