<?php namespace App\Http\Controllers;

use Cache;
use \GameQ\GameQ;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function servers() {

        if(Cache::has('serverInfo')) {
            $response = Cache::get('serverInfo');
        } else {
            $gq = new GameQ();
            $gq->addServers([
                [
                    'id' => 'mk2',
                    'type' => 'tf2',
                    'host' => '66.150.188.57:27015'
                ],
                [
                    'id' => 'mk4',
                    'type' => 'tf2',
                    'host' => '66.150.188.157:27015'
                ],
            ]);
    
            $data = $gq->process();
            $mk2 = $data['mk2'];
            $mk4 = $data['mk4'];
            $response = Cache::remember('serverInfo', now()->addMinutes(1), function() use($mk2, $mk4) {
                return [
                    'success' => true,
                    'mk2' => [
                        'name' => $mk2['hostname'],
                        'map' => $mk2['map'],
                        'players' => "{$mk2['gq_numplayers']}/{$mk2['gq_maxplayers']}",
                        'join' => $mk2['gq_joinlink']
                    ],
                    'mk4' => [
                        'name' => $mk4['hostname'],
                        'map' => $mk4['map'],
                        'players' => "{$mk4['gq_numplayers']}/{$mk4['gq_maxplayers']}",
                        'join' => $mk4['gq_joinlink']
                    ]
                ];
            });
        }

        return $response;
    }
}
