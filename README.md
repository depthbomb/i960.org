# i960.org

2018 iteration of the i960.org website, an updated application to succeed my 2014 product.

## Development

Run your usual `composer install` and `npm install`s. Make sure the required Laravel directories are writable. Run `php artisan serve` to run a local PHP server. Run `npm test` or `npm run build:d` to run a quick development processing of static assets. Run `npm run build:p` for production.