<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'PageController@index', 'as' => 'index']);

Route::get('manifest.json', ['uses' => 'PageController@appManifest', 'as' => 'manifest.json']);
Route::get('browserconfig.xml', ['uses' => 'PageController@browserConfig', 'as' => 'browserconfig.xml']);

Route::group(['prefix' => 'api'], function() {
	Route::get('servers', ['uses' => 'ApiController@servers']);
});